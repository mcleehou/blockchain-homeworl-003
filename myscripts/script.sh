
# Package Chaincode
peer lifecycle chaincode package mycc.tar.gz --path ./chaincode --lang golang --label mycc_1

# Install Chaincode
peer lifecycle chaincode install mycc.tar.gz

# Export Chaincode ID 
export CC_PACKAGE_ID=mycc_1:1f41357a5322609bbecf7b4c18aa50eafca4a4a6f20ca85d2e73bde78648b3b2

# Approved Chaincode
peer lifecycle chaincode approveformyorg --channelID workspace --name mycc --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence 1 \
--tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

# Check Commit readiness
peer lifecycle chaincode checkcommitreadiness --channelID workspace --name mycc --version 1.0 --sequence 1 --output json --init-required

# Commit Chaincode
peer lifecycle chaincode commit -o orderer1.workspace:7050 --channelID workspace --name mycc --version 1.0 --sequence 1 --init-required --peerAddresses  peer1.developers.workspace:7051 \
--tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem \
--tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt


# Query Committed Chaincode
peer lifecycle chaincode querycommitted --channelID workspace --name mycc   