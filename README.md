# hyperledger-fabric-network-from-scratch


1. Generate Crypto-Material and Up orderer and peer by run script run.sh

2. After Orders and Peers up run command: docker exec -it cli bash

3. Export Path for devPeer1 and run command join channel can see in /scripts/devPeer1.sh

4. Run script in scripts folder (accountPeer1.sh then hrPeer1.sh and then marketingPeer1.sh) for each Orgs join channel

5. Install Chaincode by follow instruction in myscript folder

6. Setup Explorer by cd to explorer folder and run command: docker-compose up -d

Explorer
    - address: http://52.221.245.88:8080
    - username: hppoc
    - password: password

CouchDB
    - address: http://52.221.245.88:5984
    - username: admin
    - password: adminpw

7. Setup Grafana by cd to test-network/moniter and logging/monitering and then run command: docker-compose up -d

Grafana 
    - address: http://52.221.245.88:3000
    - username: admingrafana
    - password: admingrafanapw